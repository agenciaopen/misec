<?php

?>

<!-- Footer -->
<footer>
<div class="container h-100">
    <div class="row">
        <div class="col-lg-12 p-0 d-flex justify-content-between footer-menu-content">
            <div class="col-md-12 col-lg-4">
                <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
                <?php $template_directory = get_template_directory_uri(); ?>
                <div class="rede_social pt-lg-3 hide-mob">
                    <div class="col-md-5 d-flex pt-lg-5">
                        <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/Facebook-Negative.svg" alt=""/></a>
                        <a href="https://twitter.com/misecintegridad" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/Twitter-Negative.svg" alt=""/></a>
                        <a href="https://www.instagram.com/misec.integridade/" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/Instagram-Negative.svg" alt=""/></a>
                        <a href="https://www.linkedin.com/company/misecintegridade/"target="blank" ><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/LinkedIn-Negative.svg" alt=""/></a>
                        <a href="https://www.youtube.com/channel/UC49GhX6-7HNlUMLwkwH3Muw" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/YouTube-Negative.svg" alt=""/></a>
                        <!-- <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank"><img class="icones_sociais pr-3" src="<?php //echo $template_directory;?>/img/TikTok-Negative.svg" alt=""/></a>
                        <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank"><img class="icones_sociais pr-3" src="<?php //echo $template_directory;?>/img/Tumblr-Negative.svg" alt=""/></a>
                        <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank" ><img class="icones_sociais pr-3" src="<?php //echo $template_directory;?>/img/Telegram-Negative.svg" alt=""/></a> -->
                    </div>
                </div>
                
            </div>
            <div class="col-md-10 d-flex justify-content-end">
                <ul class="footer-menu">         
                    <li>
                        <a href="<?php echo home_url(); ?>/quem-somos/" target="" class="pages">Quem somos</a>
                        
                        <div class="sub-menu">
                            <a href="/quem-somos#missao-e-valores">Visão e missão</a>
                            <a href="/quem-somos#movimento">Empresas participantes</a>
                            <a href="/quem-somos#parceiros">Parceiros</a>
                        </div>
                    </li>
                    <li>                                                                                                   
                        <a href="<?php echo home_url(); ?>/associe-se/" target="" class="pages">Associe-se</a>
                    
                        <div class="sub-menu">
                            <a href="/associe-se#quero-fazer-parte">Quero fazer parte</a>
                            <a href="/associe-se#informacao">Compromisso e adesão ao MISEC</a>
                            <a href="/duvidas-frequentes">Dúvidas frequentes - MISEC</a>
                        </div>
                    </li>                                                                                                        
                    <li>                                                                                                       
                        <a href="<?php echo home_url(); ?>/capacitacao-em-compliance/" target="" class="pages">Capacitação em Compliance</a>
                        
                        <div class="sub-menu">
                            <a href="/capacitacao-em-compliance#avalie">Avalie o seu programa de integridade</a>
                            <a href="/capacitacao-em-compliance#treinamento">Vídeos e treinamentos</a>
                            <a href="/capacitacao-em-compliance#cartilhas">Cartilhas</a>
                            <a href="/capacitacao-em-compliance#cartilhas">Agenda de eventos</a>
                            <a href="/capacitacao-em-compliance#noticias-e-artigos">Artigos</a>
                        </div>
                    </li>
                    <li>                                                                                                       
                        <a href="<?php echo home_url(); ?>/contato/" target="" class="pages">Contato</a>
                        <div class="col-md-5 pt-3 hide-desk">
                            <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/Facebook-Negative.svg" alt=""/></a>
                            <a href="https://twitter.com/misecintegridad" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/Twitter-Negative.svg" alt=""/></a>
                            <a href="https://www.instagram.com/misec.integridade/" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/Instagram-Negative.svg" alt=""/></a>
                            <a href="https://www.linkedin.com/company/misecintegridade/"target="blank" ><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/LinkedIn-Negative.svg" alt=""/></a>
                        </div>
                        <div class="col-md-5 pt-2 hide-desk">
                            <a href="https://www.youtube.com/channel/UC49GhX6-7HNlUMLwkwH3Muw" target="blank"><img class="icones_sociais pr-3" src="<?php echo $template_directory;?>/img/YouTube-Negative.svg" alt=""/></a>
                            <!-- <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank"><img class="icones_sociais pr-3" src="<?php //echo $template_directory;?>/img/TikTok-Negative.svg" alt=""/></a>
                            <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank"><img class="icones_sociais pr-3" src="<?php //echo $template_directory;?>/img/Tumblr-Negative.svg" alt=""/></a>
                            <a href="https://www.facebook.com/Misecintegridade-101780902200023/?" target="blank" ><img class="icones_sociais pr-3" src="<?php //echo $template_directory;?>/img/Telegram-Negative.svg" alt=""/></a> -->
                        </div>
                        
                        <div class="sub-menu">
                            <a href="/contato#fale-conosco">Fale Conosco</a>
                        </div>
                    </li>
                </ul>
            </div>
            
        </div>
        <div class="col-md-12 direitos">
            <p>Todos os direitos reservados a <strong>Open</strong></p>
        </div>
    </div>
    
</div>

</footer>

  
<?php wp_footer(); ?>

</body>

</html>
