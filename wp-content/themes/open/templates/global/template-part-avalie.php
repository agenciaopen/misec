<section class="avalie" id="avalie" style="background-image:url('<?php echo get_field( 'imagen_avalie', 'option' ) ?>');">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-5 order-2 order-md-1">
				<h2><?php the_field( 'titulo_avalie', 'option' ); ?></h2><hr>
				<p><?php the_field( 'descricao_avalie', 'option' ); ?></p><hr>
				<?php $botao_avalie = get_field( 'botao_avalie', 'option'); ?>
				<?php if ( $botao_avalie ) : ?>
					<a href="<?php echo esc_url( $botao_avalie['url'] ); ?>" target="<?php echo esc_attr( $botao_avalie['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_avalie['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
			<div class="col-md-6 order-1 order-md-2">
				<div class="hidden-desk">
				</div>
			</div>
		</div>
	</div>
</section>
