<section class="cartilhas" id="cartilhas">
    <div class="container h-100">
        <div class="row">
            <div class="list-cartilhas">
                <?php if ( have_rows( 'cartilhas', 'option' ) ) : ?>
                <?php while ( have_rows( 'cartilhas', 'option' ) ) : the_row(); ?>
                    <div class="col-md-6">
                        <div class="card-content">
                            <div class="box-img">
                            <?php $icone = get_sub_field( 'icone' ); ?>
                            <?php if ( $icone ) : ?>
                                <img src="<?php echo esc_url( $icone['url'] ); ?>" alt="<?php echo esc_attr( $icone['alt'] ); ?>" />
                            <?php endif; ?>
                            </div>
                            <div class="box-content">
                                <h3><?php the_sub_field( 'titulo' ); ?></h3><hr>
                                <p><?php the_sub_field( 'descricao' ); ?></p><hr>
                                <?php $botao = get_sub_field( 'botao' ); ?>
                                <?php if ( $botao ) : ?>
                                    <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>">
                                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao['title'] ); ?></button>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</section>