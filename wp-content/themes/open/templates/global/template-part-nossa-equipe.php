
<section class="nossa-equipe" id="associe-se" style="background-image:url('<?php echo get_field( 'imagen_nossa_equipe', 'option' ) ?>');">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-6 box-content order-2">
                <h2><?php the_field( 'titulo_nossa_equipe', 'option' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_nossa_equipe', 'option' ); ?></p><hr>

                <?php $botao_nossa_equipe = get_field( 'botao_nossa_equipe', 'option' ); ?>
                <?php if ( $botao_nossa_equipe ) : ?>
                <a href="<?php echo esc_url( $botao_nossa_equipe['url'] ); ?>" target="<?php echo esc_attr( $botao_nossa_equipe['target'] ); ?>">
                    <button class="btn btn_first mx-auto"><?php echo esc_html( $botao_nossa_equipe['title'] ); ?></button>
                </a>
                <?php endif; ?>
            </div>
            <div class="col-md-6 hidden-desk box-img order-1"> 
            </div>
        </div>
    </div>
</section>









