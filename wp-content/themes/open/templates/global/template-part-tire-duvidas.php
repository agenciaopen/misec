<section class="fale-conosco">
	<div class="container h-100">
		<div class="col-md-12">
			<h2><?php the_field( 'titulo_fale_com_a_gente', "option" ); ?></h2><hr>
			<p><?php the_field( 'subtitulo_fale_com_a_gente', "option" ); ?></p>
		</div>
		<div class="col-md-12 form-fale">
		<?php echo do_shortcode( '[contact-form-7 id="5" title="Fale com a gente"]' ); ?>
		</div>
	</div>
</section>