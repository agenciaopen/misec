<?php
/**
*
* Template Name: Quem Somos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="about" id="missao-e-valores">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <?php the_field( 'descricao_quem_somos' ); ?><hr>

                <h2><?php the_field( 'titulo_nossos_valores' ); ?></h2><hr>
                <ul class="valores">
                    <?php if ( have_rows( 'cadastro_valores' ) ) : ?>
                        <?php while ( have_rows( 'cadastro_valores' ) ) : the_row(); ?>
                            <li>
                                <?php $icone = get_sub_field( 'icone' ); ?>
                                <?php if ( $icone ) : ?>
                                    <img src="<?php echo esc_url( $icone['url'] ); ?>" alt="<?php echo esc_attr( $icone['alt'] ); ?>" />
                                <?php endif; ?>
                                <p><?php the_sub_field( 'descricao_icone' ); ?></p>
                            </li>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </ul>
                <hr>
                <?php $botao_quem_somos = get_field( 'botao_quem_somos' ); ?>
                <?php if ( $botao_quem_somos ) : ?>
                    <a href="<?php echo esc_url( $botao_quem_somos['url'] ); ?>" target="<?php echo esc_attr( $botao_quem_somos['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_quem_somos['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<!-- <section class="movimento" id="movimento">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php //the_field( 'titulo_movimento' ); ?></h2><hr>
				<p><?php //the_field( 'subtitulo_movimento' ); ?></p><hr>
			</div>
			<div class="col-md-12 galery">
				<?php //if ( have_rows( 'galerias_movimento' ) ) : ?>
					<?php //while ( have_rows( 'galerias_movimento' ) ) : the_row(); ?>
						<ul class="parceiros-carousel">
							<div class="col-md-12 p-0">
								<h6><?php //the_sub_field( 'titulo_galeria' ); ?></h6>
							</div>
							<?php //$galeria_movimento_urls = get_sub_field( 'galeria_movimento' ); ?>
							<?php //if ( $galeria_movimento_urls ) :  ?>
								<?php //foreach ( $galeria_movimento_urls as $galeria_movimento_url ): ?>
									<li>
										<img src="<?php //echo esc_url( $galeria_movimento_url ); ?>" />
									</li>
								<?php //endforeach; ?>
							<?php //else: ?>
								<hr>
								<p>Em breve</p>	
							<?php //endif; ?>
						</ul><hr>
					<?php //endwhile; ?>
				<?php //else : ?>
					<?php // no rows found ?>
				<?php //endif; ?>
			</div>
			<div class="col-md-12">
				<?php //$botao_movimento = get_field( 'botao_movimento' ); ?>
				<?php //if ( $botao_movimento ) : ?>
					<a href="<?php //echo esc_url( $botao_movimento['url'] ); ?>" target="<?php //echo esc_attr( $botao_movimento['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php //echo esc_html( $botao_movimento['title'] ); ?></button>
					</a>
				<?php //endif; ?>
			</div>
		</div>
	</div>
</section> -->

<section class="transformacao">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_transformacao' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_transformacao' ); ?></p><hr>
			</div>

			<div class="col-md-4 pt-3 galery">
				<?php if ( have_rows( 'galerias_transformacao_secretaria_executiva' ) ) : ?>
					<?php while ( have_rows( 'galerias_transformacao_secretaria_executiva' ) ) : the_row(); ?>
						<ul class="d-block">
							<div class="col-md-12 p-0">
								<h6><?php the_sub_field( 'titulo_galeria_secretaria_executiva' ); ?></h6>
							</div>
							<?php $galeria_transformacao_urls = get_sub_field( 'galeria_transformacao_secretaria_executiva' ); ?>
							<?php if ( $galeria_transformacao_urls ) :  ?>
								<?php foreach ( $galeria_transformacao_urls as $galeria_transformacao_url ): ?>
									<li><img class="pl-10 pr-10" src="<?php echo esc_url( $galeria_transformacao_url ); ?>" /></li>
								<?php endforeach; ?>
							<?php else: ?>
							<hr>
								<p>Em breve</p>							
							<?php endif; ?>
						</ul>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div> 
			
			<div class="col-md-8 pt-3 galery">
				<?php if ( have_rows( 'galerias_transformacao' ) ) : ?>
					<?php while ( have_rows( 'galerias_transformacao' ) ) : the_row(); ?>
						<ul class="galeria-carousel">
							<div class="col-md-12 p-0">
								<h6><?php the_sub_field( 'titulo_galeria' ); ?></h6>
							</div>
							<?php $galeria_transformacao_urls = get_sub_field( 'galeria_transformacao' ); ?>
							<?php if ( $galeria_transformacao_urls ) :  ?>
								<?php foreach ( $galeria_transformacao_urls as $galeria_transformacao_url ): ?>
									<li><img class="img-thumbnail" src="<?php echo esc_url( $galeria_transformacao_url ); ?>" /></li>
								<?php endforeach; ?>
							<?php else: ?>
							<hr>
								<p>Em breve</p>							
							<?php endif; ?>
						</ul>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div>
			<div class="col-md-12">
				<?php $botao_transformacao = get_field( 'botao_transformacao' ); ?>
				<?php if ( $botao_transformacao ) : ?>
					<a href="<?php echo esc_url( $botao_transformacao['url'] ); ?>" target="<?php echo esc_attr( $botao_transformacao['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_transformacao['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="about">
	<div class="container h-100">
		<div class="row">

				<h5>Outras associadas:</h5>
		</div>
		
		<div class="col-md-12 d-flex pt-5">
			<div class="row">
				<?php if( have_rows('outras_associadas') <=3 ): ?>	
				
				<?php while( have_rows('outras_associadas') ) : the_row(); ?>

				<div class="col-md-4 mb-4">
					<strong class="mt-3"> <?php the_sub_field('letra_inicial'); ?></strong>
					<p class="txt_associadas"><?php the_sub_field('associada'); ?></p>
				</div>
						
								
				<?php endwhile; ?>

				<?php else : ?>
						
				<?php endif; ?>
				
			</div>		
		</div>
		<div class="row"> 
			<?php $botao_associados = get_field( 'botao_associados' ); ?>
				<?php if ( $botao_associados ) : ?>
					<a href="<?php echo esc_url( $botao_associados['url'] ); ?>" target="<?php echo esc_attr( $botao_associados['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_associados['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
	</div>
</section>
<section class="parceiros" id="parceiros">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_parceiros' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_parceiros' ); ?></p><hr>
			</div>
			<div class="col-md-12 pt-3 galery">
				<ul class="parceiros-carousel">
					<?php $galeria_parceiros_urls = get_field( 'galeria_parceiros' ); ?>
					<?php if ( $galeria_parceiros_urls ) :  ?>
						<?php foreach ( $galeria_parceiros_urls as $galeria_parceiros_url ): ?>
							<li>
								<img src="<?php echo esc_url( $galeria_parceiros_url ); ?>" />
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
					<div class="col-md-12 p-0">
						<?php the_field( 'descricao_galeria' ); ?>
					</div>
				</ul>
			</div>
			<div class="col-md-12">
				<?php $botao_parceiros = get_field( 'botao_parceiros' ); ?>
				<?php if ( $botao_parceiros ) : ?>
					<a href="<?php echo esc_url( $botao_parceiros['url'] ); ?>" target="<?php echo esc_attr( $botao_parceiros['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_parceiros['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
 
<?php get_footer(); ?>