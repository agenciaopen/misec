<?php
/**
*
* Template Name: Politicas de privacidades
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<section class="politicas">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo get_the_title()?></h1><hr>       
                <?php echo the_content() ?>
            </div>
        </div>
    </div>
</section>




<?php get_footer(); ?>