<?php
/**
*
* Template Name: Agenda de eventos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="agenda">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12 pb-3">
                <h2><?php the_field( 'titulo_agenda' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_agenda' ); ?></p><hr>
            </div>
            <!--<?php /*<div class="col-md-12">
                <h2><?php the_field( 'titulo_eventos' ); ?></h2><hr><br>
                <ul class="tabela-de-eventos">
                    <ul class="titulo-tabela">
                        <li class="col-md-4">Nome do evento</li>
                        <li class="col-md-4">Data do evento</li>
                        <li class="col-md-4">Descrição do evento</li>
                    </ul>
                    <?php if ( have_rows( 'tabela_de_eventos' ) ) : ?>
                        <?php while ( have_rows( 'tabela_de_eventos' ) ) : the_row(); ?>
                        <ul class="list-eventos">
                            <li class="col-md-4"><?php the_sub_field( 'nome_do_evento' ); ?></li>
                            <li class="col-md-4"><?php the_sub_field( 'data_do_evento' ); ?></li>
                            <li class="col-md-4"><?php the_sub_field( 'detalhe_do_evento' ); ?></li>
                        </ul>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-md-12">
                <h2><?php the_field( 'titulo_eventos_passados' ); ?></h2><hr><br>
                <ul class="tabela-de-eventos">
                    <ul class="titulo-tabela">
                        <li class="col-md-4">Nome do evento</li>
                        <li class="col-md-4">Data do evento</li>
                        <li class="col-md-4">Descrição do evento</li>
                    </ul>
                    <?php if ( have_rows( 'tabela_de_eventos_passados' ) ) : ?>
                        <?php while ( have_rows( 'tabela_de_eventos_passados' ) ) : the_row(); ?>
                        <ul class="list-eventos">
                            <li class="col-md-4"><?php the_sub_field( 'nome_do_evento' ); ?></li>
                            <li class="col-md-4"><?php the_sub_field( 'data_do_evento' ); ?></li>
                            <li class="col-md-4"><?php the_sub_field( 'detalhe_do_evento' ); ?></li>
                        </ul>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </ul>
            </div>*/?>-->
        </div>   
    </div>    
</section>
    
<?php get_template_part( 'templates/global/template-part', 'tire-duvidas' ); ?>  
    
<?php get_template_part( 'templates/global/template-part', 'avalie' ); ?>

<?php get_footer(); ?>