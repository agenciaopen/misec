<?php
/**
*
* Template Name: Misec na web
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="noticia">
    <div class="container h-100">
        <div class="row">
            <?php get_template_part( 'templates/global/template-part', 'noticias' ); ?>
        </div>
    </div>
</section>

<?php get_template_part( 'templates/global/template-part', 'avalie' ); ?>

<section class="agenda-de-eventos" style="background-image:url('<?php echo get_field( 'imagem_agenda', 'option' ) ?>');">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-7">
            </div>
            <div class="col-md-5">
                <h2><?php the_field( 'titulo_agenda', 'option' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_agenda', 'option' ); ?></p><hr>
                <?php $botao_agenda = get_field( 'botao_agenda', 'option' ); ?>
                <?php if ( $botao_agenda ) : ?>
                    <a href="<?php echo esc_url( $botao_agenda['url'] ); ?>" target="<?php echo esc_attr( $botao_agenda['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_agenda['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
 
<?php get_footer(); ?>