<?php
/**
*
* Template Name: Cartilhas
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="list-cartilhas">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 text-center">
				<h2>Cartilhas</h2><hr>
			</div>
			<div class="col-md-12">
				<ul class="cartilhas-interna">
				<?php if ( have_rows( 'cadastro_de_cartilhas' ) ) : ?>
					<?php while ( have_rows( 'cadastro_de_cartilhas' ) ) : the_row(); ?>
						
						<li>
							<?php $arquivo = get_sub_field( 'arquivo' ); ?>
							<?php if ( $arquivo ) : ?>
								<a href="<?php echo esc_url( $arquivo['url'] ); ?>" target="_blank"><?php the_sub_field( 'nome' ); ?></a>
							<?php else : ?>
								<a href="<?php the_sub_field( 'redirecionado' ); ?>" target="_blank">
									<?php the_sub_field( 'nome' ); ?>
									</a>
							<?php endif; ?>
						</li>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
 
<?php get_footer(); ?>