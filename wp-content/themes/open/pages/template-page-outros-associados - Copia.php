<?php
/**
*
* Template Name: Outros Associados
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>


<section class="transformacao">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_transformacao' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_transformacao' ); ?></p><hr>
			</div>

			<div class="col-md-4 pt-3 galery">
				<?php if ( have_rows( 'galerias_transformacao_secretaria_executiva' ) ) : ?>
					<?php while ( have_rows( 'galerias_transformacao_secretaria_executiva' ) ) : the_row(); ?>
						<ul class="d-block">
							<div class="col-md-12 p-0">
								<h6><?php the_sub_field( 'titulo_galeria_secretaria_executiva' ); ?></h6>
							</div>
							<?php $galeria_transformacao_urls = get_sub_field( 'galeria_transformacao_secretaria_executiva' ); ?>
							<?php if ( $galeria_transformacao_urls ) :  ?>
								<?php foreach ( $galeria_transformacao_urls as $galeria_transformacao_url ): ?>
									<li><img class="pl-10 pr-10" src="<?php echo esc_url( $galeria_transformacao_url ); ?>" /></li>
								<?php endforeach; ?>
							<?php else: ?>
							<hr>
								<p>Em breve</p>							
							<?php endif; ?>
						</ul>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div> 
			
			<div class="col-md-8 pt-3 galery">
				<?php if ( have_rows( 'galerias_transformacao' ) ) : ?>
					<?php while ( have_rows( 'galerias_transformacao' ) ) : the_row(); ?>
						<ul class="galeria-carousel">
							<div class="col-md-12 p-0">
								<h6><?php the_sub_field( 'titulo_galeria' ); ?></h6>
							</div>
							<?php $galeria_transformacao_urls = get_sub_field( 'galeria_transformacao' ); ?>
							<?php if ( $galeria_transformacao_urls ) :  ?>
								<?php foreach ( $galeria_transformacao_urls as $galeria_transformacao_url ): ?>
									<li><img class="img-thumbnail" src="<?php echo esc_url( $galeria_transformacao_url ); ?>" /></li>
								<?php endforeach; ?>
							<?php else: ?>
							<hr>
								<p>Em breve</p>							
							<?php endif; ?>
						</ul>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div>
			<div class="col-md-12">
				<?php $botao_transformacao = get_field( 'botao_transformacao' ); ?>
				<?php if ( $botao_transformacao ) : ?>
					<a href="<?php echo esc_url( $botao_transformacao['url'] ); ?>" target="<?php echo esc_attr( $botao_transformacao['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_transformacao['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>


 
<?php get_footer(); ?>