<?php
/**
*
* Template Name: Fique por dentro
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="page_post fique-por-dentro">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<ul class="list-post">
					<?php 
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

					$data= new WP_Query(array(
						'post_type'     => 'post',
						'posts_per_page' => 3, // post per page
						'paged' => $paged,
					));

					if($data->have_posts()) :
						while($data->have_posts())  : $data->the_post();?>
							<li class="card box-card-<?php echo $cont; ?>">
								<div class="card-header col-md-6">
									<?php $post_thumbnail = get_field( 'post_thumbnail' ); ?>
									<?php if ( $post_thumbnail ) : ?>
										<img src="<?php echo esc_url( $post_thumbnail['url'] ); ?>" alt="<?php echo esc_attr( $post_thumbnail['alt'] ); ?>" />
									<?php endif; ?>
								</div>
								<div class="card-content col-md-6">
									<span class="date"><?php the_date(); ?></span>
									<h3><?php the_title(''); ?></h3>
									<hr>
									<p><?php the_content(); ?></p><hr>
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<button class="btn btn_first mt-4 mx-auto mb-4">CONTINUAR LENDO</button>
									</a>
								</div>
							</li>
						<?php endwhile;

						$total_pages = $data->max_num_pages;?>

						<div class="pagination">
							<?php if ($total_pages > 1){

							$current_page = max(1, get_query_var('paged'));

							echo paginate_links(array(
								'base' => get_pagenum_link(1) . '%_%',
								'format' => '/page/%#%',
								'current' => $current_page,
								'total' => $total_pages,
                                'prev_text'    => __(''),
								'next_text'    => __(' ...'),
							));
							}
							?>    
						</div>
					<?php else :?>
					<?php endif; ?>
					<?php wp_reset_postdata();?>
				</ul>
				
			</div>
		</div>
	</div>
</section>






<?php get_template_part( 'templates/global/template-part', 'cartilhas' ); ?>

<?php get_footer(); ?>