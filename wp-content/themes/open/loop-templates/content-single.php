<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<?php         $banner = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
	<section class="main_banner one_fourth pb-0" id="" style="background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo $banner;?>')"> 
		<div class="container h-100">
			<div class="row h-100 justify-content-center align-items-end">
				<div class="col-md-12 text-center">
					<?php the_title( '<h1 class="entry-title text-white">', '</h1>' ); ?>
					<h2 class="text-white"><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);?></h2>
				</div>
			</div><!--/.container-->
		</div><!--/.row-->
	</section><!--/.main_banner-->

				
	<section class="post_content pt-4">
		<div class="container h-100 align-items-center">
			<div class="row h-100 align-items-center justify-content-center border-bottom pb-3">
				<div class="col-md-6 meta">
					<p><small>Por <?php the_author(); ?>, em <?php echo get_the_date(); ?></small></p>
				</div>
				<div class="col-md-6 text-center text-lg-right">
					<?php if( has_active_subscription() ){ 	} else { ?>
						<a href="/pagamento">
							<div class="btn btn-inverse">
								Assine já
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
 	<?php  $active_sub = get_post_meta(get_the_ID(), '_wc_memberships_force_public', true); ?>
		<?php if(( get_field('liberar_conteudo') == 'sim' || has_active_subscription() || ( 'yes' == get_post_meta(get_the_ID(), '_wc_memberships_force_public', true)))){ // Current user has an active subscription ?>
			<section class="post_content pt-2">
				<div class="container h-100 align-items-center">
					<div class="row h-100 align-items-center justify-content-center border-botom">
						<div class="col-md-12">
							<?php the_content(); ?>
						</div>
						
					</div>
				</div>
			</section>		
	<?php }
	else {?>
		<section class="post_content pt-2">
			<div class="container h-100 align-items-center">
				<div class="row h-100 align-items-center justify-content-center border-botom">
					<div class="col-md-12 not_allow">
							<?php the_content(); ?>
							
					</div>
					
				</div>
			</div>
		</section>		
	<?php } ?>
	
	<section class="post_content pt-2">
		<div class="container h-100 align-items-center">
			<div class="row h-100 align-items-center justify-content-center border-botom">
				<div class="col-md-10">
					<div class="media author-box">
						<div class="media-figure">
							<?php echo get_avatar( get_the_author_meta('email'), '100' ); ?>
						</div>

						<div class="media-body row pl-4 m-0">
							<div class="col-8">
								<h3><?php the_author_posts_link(); ?></h3>
							</div>
							<div class="col-4">
								<ul class="list-inline">
									<li class="list-inline-item">
<a href="<?php the_author_meta('user_url'); ?>" class="author-website">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M4 2H20C21.1046 2 22 2.89543 22 4V20C22 21.1046 21.1046 22 20 22H4C2.89543 22 2 21.1046 2 20V4C2 2.89543 2.89543 2 4 2ZM4 4V20H20V4H4ZM13 9C12.4823 9 11.9353 9.15826 11.4521 9.45215L11 9H10V16H12V12C12 11.4243 12.594 11 13 11H14C14.406 11 15 11.4243 15 12V16H17V12C17 10.1472 15.394 9 14 9H13ZM8 8C8.55228 8 9 7.55228 9 7C9 6.44772 8.55228 6 8 6C7.44772 6 7 6.44772 7 7C7 7.55228 7.44772 8 8 8ZM7 9V16H9V9H7Z" fill="#767676"/>
									</svg>

								</a>
								<a href="<?php the_author_meta('twitter'); ?>" class="author-twitter d-none">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1195 4.50827L22.5343 4.67349L21.8983 5.948C21.5882 6.56953 21.2778 7.19105 20.967 7.81258C20.9302 7.94422 20.8654 8.05962 20.7697 8.20987C20.7296 8.27265 20.5929 8.47236 20.5865 8.48194C20.5504 8.53608 20.5237 8.57878 20.5045 8.61299V11.0015C20.5045 17.1135 14.5895 20.9974 9.00354 20.9974C7.86051 20.9974 6.99207 20.9427 5.99765 20.7257C4.36115 20.3685 3.14327 19.6587 2.58597 18.418L2.01221 17.1407L3.40659 17.0124C4.66801 16.8964 5.76169 16.6561 6.60159 16.3343C4.29577 15.9635 3.0036 14.9508 3.0036 13.0489V12.0489H4.0036C4.22331 12.0489 4.42143 12.0311 4.59854 11.9983C2.868 10.9636 2.00122 9.30379 2.00122 7.00152C2.00103 6.9034 2.00103 6.90339 2.00044 6.79847C1.99394 5.63803 2.05627 5.01797 2.37395 4.22659C2.57754 3.71941 2.87183 3.24988 3.2679 2.81967L4.02251 2L4.75617 2.83847C7.17394 5.60161 9.56395 7.27795 12.0042 7.48072C12.0146 4.93105 13.9415 3.00152 16.5043 3.00152C17.6991 3.00152 18.7828 3.45501 19.6345 4.27273C20.1006 4.36851 20.5957 4.44709 21.1195 4.50827ZM18.9086 6.16202L18.6021 6.0926L18.3904 5.86028C17.8785 5.29855 17.2359 5.00152 16.5043 5.00152C15.0414 5.00152 14.0041 6.04391 14.0041 7.50152C14.0041 7.73974 13.998 7.88942 13.9683 8.08615C13.8499 8.87116 13.4096 9.50152 12.5041 9.50152C9.50607 9.50152 6.80136 7.89542 4.16389 5.15228C4.02792 5.56561 3.99595 5.99047 4.00041 6.78727C4.00101 6.89384 4.00101 6.89384 4.00122 7.00152C4.00122 9.04953 4.83093 10.1698 6.79547 10.7942L7.49255 11.0158V11.7472C7.49255 12.6342 6.65222 13.4691 5.42268 13.8431C5.98631 14.2708 7.139 14.5015 9.00389 14.5015H10.0039V15.5015C10.0039 16.9343 8.35762 18.0561 5.87075 18.6419C6.68178 18.8903 7.76166 18.9974 9.00354 18.9974C13.618 18.9974 18.5045 15.7888 18.5045 11.0015V8.50152C18.5045 8.20774 18.5897 7.95273 18.7311 7.68759C18.7865 7.58393 18.8474 7.48509 18.9225 7.37237C18.9367 7.35115 18.9892 7.27426 19.0309 7.21279L19.1101 7.05429C19.2386 6.79745 19.3669 6.54061 19.4952 6.28377C19.2958 6.24599 19.1003 6.20541 18.9086 6.16202Z" fill="#E5E5E5"/>
									</svg>

								</a>
								<a href="<?php the_author_meta('facebook'); ?>" class="author-facebook d-none">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1195 4.50827L22.5343 4.67349L21.8983 5.948C21.5882 6.56953 21.2778 7.19105 20.967 7.81258C20.9302 7.94422 20.8654 8.05962 20.7697 8.20987C20.7296 8.27265 20.5929 8.47236 20.5865 8.48194C20.5504 8.53608 20.5237 8.57878 20.5045 8.61299V11.0015C20.5045 17.1135 14.5895 20.9974 9.00354 20.9974C7.86051 20.9974 6.99207 20.9427 5.99765 20.7257C4.36115 20.3685 3.14327 19.6587 2.58597 18.418L2.01221 17.1407L3.40659 17.0124C4.66801 16.8964 5.76169 16.6561 6.60159 16.3343C4.29577 15.9635 3.0036 14.9508 3.0036 13.0489V12.0489H4.0036C4.22331 12.0489 4.42143 12.0311 4.59854 11.9983C2.868 10.9636 2.00122 9.30379 2.00122 7.00152C2.00103 6.9034 2.00103 6.90339 2.00044 6.79847C1.99394 5.63803 2.05627 5.01797 2.37395 4.22659C2.57754 3.71941 2.87183 3.24988 3.2679 2.81967L4.02251 2L4.75617 2.83847C7.17394 5.60161 9.56395 7.27795 12.0042 7.48072C12.0146 4.93105 13.9415 3.00152 16.5043 3.00152C17.6991 3.00152 18.7828 3.45501 19.6345 4.27273C20.1006 4.36851 20.5957 4.44709 21.1195 4.50827ZM18.9086 6.16202L18.6021 6.0926L18.3904 5.86028C17.8785 5.29855 17.2359 5.00152 16.5043 5.00152C15.0414 5.00152 14.0041 6.04391 14.0041 7.50152C14.0041 7.73974 13.998 7.88942 13.9683 8.08615C13.8499 8.87116 13.4096 9.50152 12.5041 9.50152C9.50607 9.50152 6.80136 7.89542 4.16389 5.15228C4.02792 5.56561 3.99595 5.99047 4.00041 6.78727C4.00101 6.89384 4.00101 6.89384 4.00122 7.00152C4.00122 9.04953 4.83093 10.1698 6.79547 10.7942L7.49255 11.0158V11.7472C7.49255 12.6342 6.65222 13.4691 5.42268 13.8431C5.98631 14.2708 7.139 14.5015 9.00389 14.5015H10.0039V15.5015C10.0039 16.9343 8.35762 18.0561 5.87075 18.6419C6.68178 18.8903 7.76166 18.9974 9.00354 18.9974C13.618 18.9974 18.5045 15.7888 18.5045 11.0015V8.50152C18.5045 8.20774 18.5897 7.95273 18.7311 7.68759C18.7865 7.58393 18.8474 7.48509 18.9225 7.37237C18.9367 7.35115 18.9892 7.27426 19.0309 7.21279L19.1101 7.05429C19.2386 6.79745 19.3669 6.54061 19.4952 6.28377C19.2958 6.24599 19.1003 6.20541 18.9086 6.16202Z" fill="#E5E5E5"/>
									</svg>
								</a>
									</li>
								</ul>
							</div>
							<div class="row border-top py-3 m-0">
								<div class="col-md-8">
									<p><?php the_author_meta('description'); ?></p>

								</div>
								<div class="col-md-4">
									<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" title="<?php echo esc_attr( get_the_author() ); ?>">VER MAIS</a>
								</div>
							</div>

						</div>

					</div>
				</div>
				
			</div>
		</div>
	</section>	

	<section class="related">
         <div class="container h-100">
			 <div class="row justify-content-start align-items-start mb-5">
				 		<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>
			 </div>
             <div class="row justify-content-start align-items-start list_posts">
        
                <?php $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
					if( $related ){ ?>
						<div class="col-md-12 text-center text-xl-left mb-5">
                    		<h3>Veja também</h3>
                		</div>
						<?php foreach( $related as $post ) {
                        setup_postdata($post); ?>
                      	<div class="row m-0 align-items-center h-100 item w-100">
							<div class="col-md-12 card">
								<div class="card-body p-0">
									<div class="row m-0 align-items-center">
										<div class="col-md-8 pl-lg-5">
											<div class="col-md-12">
												<h3 class="mt-4"><a href="<?php echo get_permalink() ?>"><?php the_title()?></a></h3>
											</div>
											<div class="col-md-12 meta">
												<p><small>Por <?php the_author(); ?>, em <?php echo get_the_date(); ?></small></p>
												<hr>
											</div>
											<div class="col-md-10 description">
												<p>
													<a href="<?php echo get_permalink() ?>">
														<?php echo wp_trim_words( get_the_content(), 50, '...' );?>
													</a>
												</p>
											</div>
											<div class="card-footer p-0 pt-3">
												<div class="col-md-12">
													<a href="<?php echo get_permalink() ?>" class="text-uppercase">leia mais ></a>
												</div>
											</div>

										</div>
										<div class="col-md-4 p-0 text-center" >
											<a href="<?php echo get_permalink() ?>" class="img_link">    
												<span class="badge badge-primary">Artigo</span>
												<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post), 'thumbnail' ); ?>
												<div class="col-md-12 p-0 bg_post" style="background-image: url('<?php echo $url; ?>');">

												</div>
											</a>
										</div>
										
										
									</div>
								</div>
							
							</div>
						</div>           
					<?php }}
                wp_reset_postdata(); ?>
                       
				<div class="col-md-12 text-center mt-5">
					<a href="/artigos" class="text-uppercase text-underline">ver todos os artigos ></a>
				</div>
             </div>
        </div>
    </section>
		
         


</article><!-- #post-## -->
