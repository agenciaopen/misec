<?php
get_header();

?>

    <?php
    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();
            ?>
            <?php 

        if ( wp_is_mobile() ) :
            $featured_img_url = get_field( 'post_banner_mobile');
        else :
            $featured_img_url = get_field( 'post_banner_desktop');
        endif;
    
        ?>
        <?php if( get_field('post_titulo_principal', $page_ID) ): ?>
            <?php $title = get_field('post_titulo_principal', $page_ID); ?>
        <?php else: ?>
            <?php $title = get_the_title(); ?>
        <?php endif; ?>
        
        <section class="main post" style="background-image: url('<?php echo $featured_img_url;?>');">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center">
                </div>
            </div>
        </section><!-- /.main -->
            <section class="interna-blog-post">
                <div class="container h-100">
                    <div class="row">
                        <div class="col-md-12 pb-4">
                            <a class="voltar" href="/fique-por-dentro">
                                < voltar para as outras matérias
                            </a>
                        </div>
                        <div class="col-md-12 post-materia">
                            <span><?php echo get_the_date(); ?></span><hr>
                            <h3><?php echo $title;?></h3><hr>
                            <article id="<?php echo $post->ID; ?>" class="section section-text">
                                <?php the_field( 'post_materia' ); ?>
                            </article>
                        </div>
                    </div>
                    
                </div>
            </section><!-- /.blog-post -->
                 
<?php
				endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
			endif;
				?>


	<?php get_footer(); ?>
